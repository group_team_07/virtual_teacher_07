package com.example.virtual_teacher.models.dtos;

import com.example.virtual_teacher.models.validators.ValidPassword;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UpdateUserByRegularDto {

    @NotNull
    @Size(min = 4, max = 32, message = "First name must be between 4 and 32 symbols")
    private String firstName;

    @NotNull
    @Size(min = 4, max = 32, message = "Last name must be between 4 and 32 symbols")
    private String lastName;

    @ValidPassword
    private String password;
    private String imgUrl;

    public UpdateUserByRegularDto() {}

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
